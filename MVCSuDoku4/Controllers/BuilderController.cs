﻿using System;
using System.Web.Mvc;
using MVCSuDoku4.Model;
using MVCSuDoku4.ServiceLayer;

namespace MVCSuDoku4.Controllers
{
	/// <summary>
	/// Description of BuilderController.
	/// </summary>
	public class BuilderController : Controller
	{
		private IPuzzleSaver puzzleSaver;
		private IPuzzleService puzzleService;
		
		public BuilderController(IPuzzleSaver saver, IPuzzleService service)
		{
			puzzleSaver = saver;
			puzzleService = service;
			
			// Load Constants into ViewData for _BoardView partial view
			ViewData["BoardSize"] = Constants.BoardSize;
			ViewData["BlockSize"] = Constants.BlockSize;
		}
		
		public ActionResult CreatePuzzleSetup()
		{
			FullBoard board = new FullBoard() { BoardList = puzzleService.SetupBoard() };
			return View("BuilderView", board);
		}
		
		public ActionResult UpdatePuzzleSetup(FullBoard board)
		{
			board.Status = puzzleService.GetPuzzleStatus(board.BoardList);
			return View("BuilderView", board);
		}
		
		[HttpPost]
		public ActionResult SavePuzzleSetup(FullBoard board)
		{
			int puzzleNumber = board.BoardNumber;
			puzzleSaver.SavePuzzleSetup(board.BoardList, ref puzzleNumber);
			board.BoardNumber = puzzleNumber;
			board.Status = puzzleService.GetPuzzleStatus(board.BoardList);
			
			// Update the model's BorderNumber by a redirect to an HttpGet action
			TempData["Board"] = board;
			return RedirectToAction("SavePuzzleSetup");
		}
		
		[HttpGet]
		public ActionResult SavePuzzleSetup()
		{
			return View("BuilderView", (FullBoard)TempData["Board"]);
		}
	}
}