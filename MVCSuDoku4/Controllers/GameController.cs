﻿using System;
using System.Web.Mvc;
using MVCSuDoku4.Model;
using MVCSuDoku4.ServiceLayer;

namespace MVCSuDoku4.Controllers
{
	/// <summary>
	/// Description of GameController.
	/// </summary>
	public class GameController : Controller
	{
		private IPuzzleLoader puzzleLoader;
		private IPuzzleSaver puzzleSaver;
		private IPuzzleService puzzleService;
		
		public GameController(IPuzzleLoader loader, IPuzzleSaver saver, IPuzzleService service)
		{
			puzzleLoader = loader;
			puzzleSaver = saver;
			puzzleService = service;
			
			// Load Constants into ViewData for partial view
			ViewData["BoardSize"] = Constants.BoardSize;
			ViewData["BlockSize"] = Constants.BlockSize;
		}
			
		public ActionResult NewGame()
		{
			FullBoard board = new FullBoard() { BoardList = puzzleService.SetupBoard() };
			int puzzleNumber;
			puzzleLoader.LoadNewPuzzle(board.BoardList, out puzzleNumber);
			board.BoardNumber = puzzleNumber;
			return View("GameView", board);
		}
		
		public ActionResult UpdateGame(FullBoard board)
		{
			board.Status = puzzleService.GetPuzzleStatus(board.BoardList);
			return View("GameView", board);
		}
		
		public ActionResult ResetGame(FullBoard board)
		{
			board.BoardList = puzzleService.SetupBoard();
			puzzleLoader.ReloadPuzzle(board.BoardList, board.BoardNumber);
			return View("GameView", board);
		}
		
		public ActionResult SaveGame(FullBoard board)
		{
			puzzleSaver.SaveGame(board.BoardList, board.BoardNumber);
			board.Status = puzzleService.GetPuzzleStatus(board.BoardList);
			return View("GameView", board);
		}
		
		public ActionResult LoadGame()
		{
			FullBoard board = new FullBoard() { BoardList = puzzleService.SetupBoard() };
			int puzzleNumber;
			puzzleLoader.LoadSavedPuzzle(board.BoardList, out puzzleNumber);
			board.Status = puzzleService.GetPuzzleStatus(board.BoardList);
			board.BoardNumber = puzzleNumber;
			return View("GameView", board);
		}
	}
}