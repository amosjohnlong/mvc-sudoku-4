﻿/*
 * Created by SharpDevelop.
 * User: Amos
 * Date: 5/21/2016
 * Time: 12:18 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.IO;
using System.Xml.Linq;

namespace MVCSuDoku4.DataLayer
{
	/// <summary>
	/// Description of XmlFilePuzzleRepository.
	/// </summary>
	public class XmlFilePuzzleRepository : IPuzzleRepository
	{
		private const string puzzleSetupXmlString = "PuzzleSetup.xml";
		private const string savedGameXmlString = "SavedGame.xml";
		
		readonly private string puzzleSetupXmlPath = string.Empty;
		readonly private string savedGameXmlPath = string.Empty;
		
		public XmlFilePuzzleRepository()
		{
			puzzleSetupXmlPath = Path.Combine(AppDomain.CurrentDomain.GetData("APPBASE").ToString(), puzzleSetupXmlString);
			savedGameXmlPath = Path.Combine(AppDomain.CurrentDomain.GetData("APPBASE").ToString(), savedGameXmlString);
		}
		
		public XDocument LoadPuzzleSetupXDoc()
		{
			return XDocument.Load(puzzleSetupXmlPath);
		}
		
		public XDocument LoadSavedGameXDoc()
		{
			return XDocument.Load(savedGameXmlPath);
		}
		
		public void SavePuzzleSetupXDoc(XDocument xDoc)
		{
			xDoc.Save(puzzleSetupXmlPath);
		}
		
		public void SaveSavedGameXDoc(XDocument xDoc)
		{
			xDoc.Save(savedGameXmlPath);
		}
	}
}
