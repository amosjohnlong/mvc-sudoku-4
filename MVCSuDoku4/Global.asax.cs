﻿/*
 * Created by SharpDevelop.
 * User: Amos
 * Date: 5/9/2016
 * Time: 7:49 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using MVCSuDoku4.DataLayer;
using MVCSuDoku4.ServiceLayer;
using Microsoft.Practices.Unity;

namespace MVCSuDoku4
{
	public class MvcApplication : HttpApplication
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.Ignore("{resource}.axd/{*pathInfo}");
			
			routes.MapRoute("Default", "{controller}/{action}/{id}",
				new { controller = "Home", action = "Index", id = UrlParameter.Optional });
		}
		
		protected void Application_Start()
		{
			RegisterRoutes(RouteTable.Routes);
			SetupUnityFactory();
		}
		
		private static void SetupUnityFactory()
		{
			IUnityContainer container = new UnityContainer();
			container.RegisterType<IPuzzleRepository, XmlFilePuzzleRepository>();
			container.RegisterType<IPuzzleLoader, XDocPuzzleLoader>();
			container.RegisterType<IPuzzleSaver, XDocPuzzleSaver>();
			container.RegisterType<IPuzzleService, PuzzleService>();
			
			UnityControllerFactory factory = new UnityControllerFactory(container);
			ControllerBuilder.Current.SetControllerFactory(factory);
		}
	}
}
