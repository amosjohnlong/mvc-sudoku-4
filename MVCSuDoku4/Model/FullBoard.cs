﻿/*
 * Created by SharpDevelop.
 * User: Amos
 * Date: 5/11/2016
 * Time: 12:55 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using MVCSuDoku4.ServiceLayer;

namespace MVCSuDoku4.Model
{
	/// <summary>
	/// A Sudoku board
	/// </summary>
	public class FullBoard
	{
		public List<Cell> BoardList { get; set; }
		public int BoardNumber { get; set; }
		public PuzzleStatus Status { get; set; }
		
		public FullBoard()
		{
			BoardList = new List<Cell>();
			Status = PuzzleStatus.Normal;
		}
	}
}
