﻿/*
 * Created by SharpDevelop.
 * User: Amos
 * Date: 5/9/2016
 * Time: 9:30 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace MVCSuDoku4.ServiceLayer
{
	/// <summary>
	/// A cell on a Sudoku board.
	/// </summary>
	public class Cell
	{
		public int XCoordinate { get; set; }
		public int YCoordinate { get; set; }
		public int BlockNumber { get; set; }
		[Range(1, Constants.BoardSize, ErrorMessage = Constants.CellErrorMessage)]
		public int? Value { get; set; }
		
		public Cell()
		{
			Value = null;
		}
	}
}
