﻿/*
 * Created by SharpDevelop.
 * User: Amos
 * Date: 6/11/2016
 * Time: 11:42 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace MVCSuDoku4.ServiceLayer
{
	/// <summary>
	/// Description of Constants.
	/// </summary>
	internal static class Constants
	{
		internal const int BlockSize = 3;
		internal const int BoardSize = 9;
		internal const string CellErrorMessage = "Choose a number 1 through 9";
	}
}
