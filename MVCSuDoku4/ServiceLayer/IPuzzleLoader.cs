﻿/*
 * Created by SharpDevelop.
 * User: Amos
 * Date: 6/22/2016
 * Time: 7:30 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;

namespace MVCSuDoku4.ServiceLayer
{
	/// <summary>
	/// Description of IPuzzleLoader.
	/// </summary>
	public interface IPuzzleLoader
	{
		void LoadNewPuzzle(List<Cell> cellList, out int puzzleNumber);
		
		void ReloadPuzzle(List<Cell> cellList, int puzzleNumber);

		void LoadSavedPuzzle(List<Cell> cellList, out int puzzleNumber);
	}
}
