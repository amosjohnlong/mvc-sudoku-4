﻿/*
 * Created by SharpDevelop.
 * User: Amos
 * Date: 6/22/2016
 * Time: 7:51 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;

namespace MVCSuDoku4.ServiceLayer
{
	/// <summary>
	/// Description of IPuzzleSaver.
	/// </summary>
	public interface IPuzzleSaver
	{
		void SaveGame(List<Cell> cellList, int puzzleNumber);
		
		void SavePuzzleSetup(List<Cell> cellList, ref int puzzleNumber);
	}
}
