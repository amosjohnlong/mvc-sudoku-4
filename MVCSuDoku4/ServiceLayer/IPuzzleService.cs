﻿/*
 * Created by SharpDevelop.
 * User: Amos
 * Date: 6/22/2016
 * Time: 9:14 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;

namespace MVCSuDoku4.ServiceLayer
{
	/// <summary>
	/// Description of IPuzzleService.
	/// </summary>
	public interface IPuzzleService
	{
		List<Cell> SetupBoard();
		
		PuzzleStatus GetPuzzleStatus(List<Cell> cellList);
	}
}
