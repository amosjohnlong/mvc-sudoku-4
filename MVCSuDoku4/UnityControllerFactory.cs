﻿/*
 * Created by SharpDevelop.
 * User: Amos
 * Date: 6/22/2016
 * Time: 7:20 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.Practices.Unity;

namespace MVCSuDoku4
{
	/// <summary>
	/// Description of UnityControllerFactory.
	/// </summary>
	public class UnityControllerFactory : DefaultControllerFactory
	{
		private readonly IUnityContainer container;
		
		public UnityControllerFactory(IUnityContainer unityContainer)
		{
			container = unityContainer;
		}
		
		protected override IController GetControllerInstance(RequestContext context, Type controllerType)
		{
			return (controllerType == null) ? null : container.Resolve(controllerType) as IController;
		}
		
		public override void ReleaseController(IController controller)
        {
            container.Teardown(controller);
            base.ReleaseController(controller);
        }
	}
}
